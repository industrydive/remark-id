import fs from 'fs-extra';
import path from 'path';
import mustache from 'mustache';

const INPUT_DIR = './input';
const OUTPUT_DIR = './output';
const NAME_REGEX = /(.*)\.md/;
const TEMPLATE_FILENAME = 'presentation.mustache';
const STYLES_FILENAME = 'styles.css';

const createOutputDirectory = async (filename) => {
  const outputDirectoryName = filename.match(NAME_REGEX)[1];
  const outputDirectoryPath = path.join(OUTPUT_DIR, outputDirectoryName);
  await fs.ensureDir(outputDirectoryPath);
  return outputDirectoryPath;
};

const createPresentationFile = async (outputDirectoryPath, markdownString, templateHtmlString) => {
  // get the output string
  const htmlString = mustache.render(templateHtmlString, { markdown: markdownString });
  const outputFilePath = path.join(outputDirectoryPath, 'presentation.html');
  await fs.writeFile(outputFilePath, htmlString);
};

const createStylesFile = async (outputDirectoryPath) => {
  const stylesFilePath = path.join(outputDirectoryPath, STYLES_FILENAME);
  await fs.copyFile(STYLES_FILENAME, stylesFilePath);
};

const getMarkdownString = async (filename) => {
  // get the markdown inside them
  const filePath = path.join(INPUT_DIR, filename);
  const markdownString = await fs.readFile(filePath, {encoding: 'utf-8'});
  return markdownString;
};

const buildRemarkFiles = async () => {
  // read in the md files from the input directory
  const markdownFiles = await fs.readdir(INPUT_DIR);

  // get the template HTML
  const templateHtmlString = await fs.readFile(TEMPLATE_FILENAME, {encoding: 'utf-8'});
  markdownFiles.filter(filename => filename.match(NAME_REGEX)).forEach(async (filename) => {
    const markdownString = await getMarkdownString(filename);
    const outputDirectoryPath = await createOutputDirectory(filename);
    await createPresentationFile(outputDirectoryPath, markdownString, templateHtmlString);
    await createStylesFile(outputDirectoryPath);
  });
};

buildRemarkFiles(INPUT_DIR, OUTPUT_DIR);
