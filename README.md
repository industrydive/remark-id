# remark-id

## How to use

- Manual
  - Copy `presentation.html` and `styles.css` into your project
  - Write your slideshow in `textarea.source`
  - Visit the `presentation.html` file from a browser
- Automated
  - Write your slideshow in a `.md` file in `input`
  - Run `npm run build`
  - Copy `presentation.html` and `styles.css` from the `output/{filename}` directory into your project
  - Visit the `presentation.html` file from a browser
